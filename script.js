"use strict";

const array = ['hello', 'world', 23, '23', null];
const typeOfData = 'string';

function filterBy(array, typeOfData) {
    const result = [];
    array.forEach((item) => {
        if (typeof(item) !== typeOfData){
            if (!(item === null && typeOfData === 'null')){
                result.push(item);
            }
        }
    });
    return result;
}

console.log(filterBy(array, typeOfData));